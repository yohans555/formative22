package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name="IndexBot")
public class IndexBot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String body,subbody;

    public IndexBot(){}

    public IndexBot(int id, String body, String subbody) {
        this.id = id;
        this.body = body;
        this.subbody = subbody;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubbody() {
        return subbody;
    }

    public void setSubbody(String subbody) {
        this.subbody = subbody;
    }
}
