package com.nexsoft.app.model;
import javax.persistence.*;

@Entity
@Table(name="AboutRight")
public class AboutRight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String imageurl;

    public AboutRight(){}


    public AboutRight(int id, String imageurl) {
        this.id = id;
        this.imageurl = imageurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    
}
