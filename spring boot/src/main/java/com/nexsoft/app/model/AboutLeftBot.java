package com.nexsoft.app.model;
import javax.persistence.*;

@Entity
@Table(name="AboutLeftBot")
public class AboutLeftBot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String heading,body;

    public AboutLeftBot(){}

    public AboutLeftBot(int id, String heading, String body) {
        this.id = id;
        this.heading = heading;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    
}
