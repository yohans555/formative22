package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name="IndexMid")
public class IndexMid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String heading,body;

    public IndexMid(){}

    

    public IndexMid(int id, String heading, String body) {
        this.id = id;
        this.heading = heading;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    
}
