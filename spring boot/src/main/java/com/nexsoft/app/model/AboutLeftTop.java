package com.nexsoft.app.model;
import javax.persistence.*;

@Entity
@Table(name="AboutLeftTop")
public class AboutLeftTop {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String heading,body,subbody;
  

    public AboutLeftTop(){}


    public AboutLeftTop(int id, String heading, String body, String subbody) {
        this.id = id;
        this.heading = heading;
        this.body = body;
        this.subbody = subbody;
    }


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getHeading() {
        return heading;
    }


    public void setHeading(String heading) {
        this.heading = heading;
    }


    public String getBody() {
        return body;
    }


    public void setBody(String body) {
        this.body = body;
    }


    public String getSubbody() {
        return subbody;
    }


    public void setSubbody(String subbody) {
        this.subbody = subbody;
    }

}
