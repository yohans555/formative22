package com.nexsoft.app.model;

import javax.persistence.*;

@Entity
@Table(name="indextop")
public class  IndexTop{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String imageurl,heading,body;

    public IndexTop(){}

    

    public IndexTop(int id, String imageurl, String heading, String body) {
        this.id = id;
        this.imageurl = imageurl;
        this.heading = heading;
        this.body = body;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    

    
}
