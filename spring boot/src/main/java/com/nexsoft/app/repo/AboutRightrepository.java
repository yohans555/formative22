package com.nexsoft.app.repo;


import com.nexsoft.app.model.AboutRight;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AboutRightrepository extends JpaRepository<AboutRight, Integer> {
}


