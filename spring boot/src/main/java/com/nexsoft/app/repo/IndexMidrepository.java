package com.nexsoft.app.repo;


import com.nexsoft.app.model.IndexMid;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IndexMidrepository extends JpaRepository<IndexMid, Integer> {
}


