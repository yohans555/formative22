package com.nexsoft.app.repo;


import com.nexsoft.app.model.IndexTop;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IndexToprepository extends JpaRepository<IndexTop, Integer> {
}


