package com.nexsoft.app.repo;


import com.nexsoft.app.model.AboutLeftBot;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AboutLeftBotrepository extends JpaRepository<AboutLeftBot, Integer> {
}


