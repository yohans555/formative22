package com.nexsoft.app.repo;


import com.nexsoft.app.model.AboutLeftTop;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AboutLeftToprepository extends JpaRepository<AboutLeftTop, Integer> {
}


