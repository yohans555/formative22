package com.nexsoft.app.repo;


import com.nexsoft.app.model.IndexBot;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IndexBotrepository extends JpaRepository<IndexBot, Integer> {
}


