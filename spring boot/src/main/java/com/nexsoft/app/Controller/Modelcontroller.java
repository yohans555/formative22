package com.nexsoft.app.Controller;

import java.util.List;

import com.nexsoft.app.model.AboutLeftBot;
import com.nexsoft.app.model.AboutLeftTop;
import com.nexsoft.app.model.AboutRight;
import com.nexsoft.app.model.IndexBot;
import com.nexsoft.app.model.IndexMid;
import com.nexsoft.app.model.IndexTop;
import com.nexsoft.app.repo.AboutLeftBotrepository;
import com.nexsoft.app.repo.AboutLeftToprepository;
import com.nexsoft.app.repo.AboutRightrepository;
import com.nexsoft.app.repo.IndexBotrepository;
import com.nexsoft.app.repo.IndexMidrepository;
import com.nexsoft.app.repo.IndexToprepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Modelcontroller {
    @Autowired
    AboutLeftBotrepository aboutLeftBotrepository;
    @Autowired
    AboutLeftToprepository aboutLeftToprepository;
    @Autowired
    AboutRightrepository aboutRightrepository;
    @Autowired
    IndexBotrepository indexBotrepository;
    @Autowired
    IndexMidrepository indexMidrepository;
    @Autowired
    IndexToprepository indexToprepository;



    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/aboutleftbot", method = RequestMethod.POST, consumes = "application/json")
    public void add1(@RequestBody AboutLeftBot model ){
        aboutLeftBotrepository.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/aboutlefttop", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody AboutLeftTop model ){
        aboutLeftToprepository.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/aboutright", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody AboutRight model ){
        aboutRightrepository.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/indextop", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody IndexTop model ){
        indexToprepository.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/indexmid", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody IndexMid model ){
        indexMidrepository.save(model);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value ="/save/indexbot", method = RequestMethod.POST, consumes = "application/json")
    public void add(@RequestBody IndexBot model ){
        indexBotrepository.save(model);
    }


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/aboutleftbot", produces = "application/json")
    public List<AboutLeftBot> getData1(){
                return aboutLeftBotrepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/aboutlefttop", produces = "application/json")
    public List<AboutLeftTop> getData2(){
                return aboutLeftToprepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/aboutright", produces = "application/json")
    public List<AboutRight> getData3(){
                return aboutRightrepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/indexbot", produces = "application/json")
    public List<IndexBot> getData4(){
                return indexBotrepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/indexmid", produces = "application/json")
    public List<IndexMid> getData5(){
                return indexMidrepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping(value ="/get/indextop", produces = "application/json")
    public List<IndexTop> getData6(){
                return indexToprepository.findAll();
    }
}
