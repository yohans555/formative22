import React from 'react';
import AboutRight from '../../Component/AboutRight.js';
import AboutLefttop from '../../Component/AbotLeftTop';
import SubHeaderContact from '../../Component/SubHeaderContact.js';
import Footer from '../../Template/Footer.js';
import '../../Style/contact.css'
import AboutLeftBot from '../../Component/AboutLeftBot.js';

class Contact extends React.Component {
    render() {
        return (
            <div>
                <SubHeaderContact/>
                
                <div class="grid-container" style={{width:'60%',marginLeft:'20%',marginRight:'20%'}}>
                    <div class="Right"><AboutRight /></div>
                    <div class="left-top"> <AboutLefttop /></div>
                    <div class="left-bot"> <AboutLeftBot /></div>
                </div>
                <Footer />
            </div>

        )
    }
}

export default Contact;