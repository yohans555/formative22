import React from 'react';
import Service from '../../Service/Service';

class Testcomponent extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("aboutlefttop").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render(){
        return (
            <div>
                <table>

                    <thead>
                        <tr>
                            <td>body</td>
                            <td>heading</td>
                            <td>subbody</td>
                        </tr>
                    </thead>

                    <tbody> 
                        {
                            this.state.datas.map(
                                data =>
                                <tr key= {data.id}>
                                    <td>{data.body}</td>
                                    <td>{data.heading}</td>
                                    <td>{data.subbody}</td>
                                </tr>
                                )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export  {Testcomponent};   