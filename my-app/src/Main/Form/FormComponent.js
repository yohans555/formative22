// component
import React from "react";
import ReactDOM from "react-dom";
import Service from "../../Service/Service";
import OnchangeService from '../../Service/OnChangehandler'
import Footer from '../../Template/Footer.js';
import './FormComponent.css';

class FormComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        imageurl: props.imageurl,
        body: props.body,
        heading: props.heading,
        subbody: props.subbody,
        target: props.target,
      },
    };
  }

  handleButtonClicked() {
    alert("data saved!");
    const elems = document.getElementsByClassName('form-input');
   
    let target = this.state.data.target;
    let jsondata = Service.getJson(this.state,target);
    Service.saveData(target, jsondata);
    for (let index = 0; index < elems.length; index++) {
      const element =elems[index];
      element.value='';
    }
    this.state.data.body=''
    this.state.data.imageurl=''
    this.state.data.heading=''
    this.state.data.subbody=''
  }

  render() {
    return (
    <div >
      <div class="overlay">
        <div class="con">
          <div class="form">
            <header class="head-form">
              <h2 style={{textAlign:'center'}}>Form content</h2>
            </header>
            <br></br>
            <input
              class="form-input"
              type="text"
              id='body'
              placeholder="body"
              value={this.state.data.body}
              required
              onChange={OnchangeService.handleBodyChanged.bind(this)}
            />
            <br />
            <br />
            <input
              class="form-input"
              type="text"
              placeholder="heading"
              id='heading'
              value={this.state.data.heading}
              required
              onChange={OnchangeService.handleHeadChanged.bind(this)}
            />
            <br />
            <br />
            <input
              class="form-input"
              type="text"
              placeholder="imageurl"
              id='imageurl'
              value={this.state.data.imageurl}
              required
              onChange={OnchangeService.handleImageChanged.bind(this)}
            />
            <input
              class="form-input"
              type="text"
              placeholder="sub body"
              id='subbody'
              value={this.state.data.subbody}
              onChange={OnchangeService.handleSubChanged.bind(this)}
            /> <br></br>
            <select
              class="form-input"
              type="text"
              placeholder="target"
              value={this.state.data.target}
              required
              onChange={OnchangeService.handleTargetChanged.bind(this)}
            >
              <option value="indexmid">indexmid</option>
              <option value="indextop">indextop</option>
              <option value="indexbot">indexbot</option>
              <option value="aboutleftbot">aboutleftbot</option>
              <option value="aboutlefttop">aboutlefttop</option>
              <option value="aboutright">aboutright</option>

            </select>

            <button
              class="log-in"
              onClick={this.handleButtonClicked.bind(this)}
            >
              Submit
            </button>
          </div>
        </div>
      </div>
      <Footer/>
    </div>
    
    );
  }
}

export default FormComponent;
