import React from 'react';
import SubHeaderIndex from '../../Component/SubHeaderIndex.js';
import ImageSlider from '../../Component/ImageSlider.js';
import IndexMid from '../../Component/IndexMid.js';
import Footer from '../../Template/Footer.js';
import IndexBot from '../../Component/IndexBot.js';
import IndexTop from '../../Component/IndexTop.js';

class IndexPage extends React.Component {
    render() {
        return (
            <div >
                <ImageSlider/>
                <SubHeaderIndex/>
                <div style={{width:'60%',marginLeft:'20%',marginRight:'20%'}}>
                    <IndexTop />
                    <IndexMid />
                    <IndexBot />
                </div>
                <Footer />
            </div>
        )
    }
}

export default IndexPage;