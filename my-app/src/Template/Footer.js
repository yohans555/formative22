import React from 'react';
import './TemplateCss/Footer.css';
import facebook from '../img/facebook.png';
import twitter from '../img/twitter.png';
import flickr from '../img/flickr.png';
import deviantart from '../img/deviantart.png';
import dribbble from '../img/dribbble.png';

class Footer extends React.Component {
    render() {
        return (
            <div>
                <div className='footer'>
                    <div className='column' style={{marginLeft:'285px', marginRight:'-100px', color:'#cbc8c3'}}>
                            <h4>ABOUT US</h4>
                            <p style={{fontSize: '10px', color: '#4b5458'}}>Our goal is to keep client statisfied!</p>
                    </div>
                    <div className='column' style={{marginRight:'-100px', color:'#cbc8c3'}}>
                            <h4>GET SOCIAL</h4>
                            <span>
                            <a href='https://www.facebook.com/' alt='fb'><img src={facebook}></img></a>
                            <a href='https://www.twitter.com/' alt='tw'><img src={twitter}></img></a>
                            <a href='https://www.flickr.com/' alt='fl'><img src={flickr}></img></a>
                            <a href='https://www.deviantart.com/' alt='da'><img src={deviantart}></img></a>
                            <a href='https://www.dribbble.com/' alt='db'><img src={dribbble}></img></a>
                            </span>
                    </div>
                    <div className='column' style={{color:'#cbc8c3'}}>
                            <h4>NEWSLETTER</h4>
                            <input className='rFooter' type='text' placeholder='Enter your email address'></input>
                            <input className='rFooter' type='submit' value='Go'></input>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;