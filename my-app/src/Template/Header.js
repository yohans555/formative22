import React from 'react';
import './TemplateCss/Header.css';

class Header extends React.Component {
    render() {
        return (
            <div>
                <nav className='navBar'>
                    <ul className='navLeft'>
                        <h2 style={{marginLeft: '250px'}}>Nexsoft design task</h2>
                    </ul>
                    <ul className='navRight'>
                        <a href="/form" style={{marginRight:'290px'}}>Form</a>
                        <a href="/contact">Contact</a>
                        <a>Features</a>
                        <a>Pages</a>
                        <a>Blog</a>
                        <a>Portofolio</a>
                        <a href="/">Home</a>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Header;