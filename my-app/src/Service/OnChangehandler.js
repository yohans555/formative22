class OnchangeService {
      handleBodyChanged(event) {
    var data = this.state.data;
    data.body = event.target.value;
    this.setState({ data: data });
  }

  handleHeadChanged(event) {
    var data = this.state.data;
    data.heading = event.target.value;

    this.setState({ data: data });
  }

  handleImageChanged(event) {
    var data = this.state.data;
    data.imageurl = event.target.value;

    this.setState({ data: data });
  }

  handleSubChanged(event) {
    var data = this.state.data;
    data.subbody = event.target.value;

    this.setState({ data: data });
  }

  handleTargetChanged(event) {
    var data = this.state.data;
    data.target = event.target.value;

    switch (event.target.value) {
      case "aboutright":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('body').hidden=true
        document.getElementById('heading').hidden=true
        document.getElementById('subbody').hidden=true
        break;

      case "indexmid":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('imageurl').hidden=true
        document.getElementById('subbody').hidden=true
        break;

      case "aboutlefttop":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('imageurl').hidden=true
        break;
      
      case "aboutleftbot":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('imageurl').hidden=true
        document.getElementById('subbody').hidden=true

        break;
      
      case "indexbot":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('imageurl').hidden=true
        document.getElementById('heading').hidden=true
        break;
      
      case "indextop":
        var inputs = document.getElementsByClassName('form-input');
        for(var i = 0; i < inputs.length; i++) {
            inputs[i].hidden = false;
        }
        document.getElementById('subbody').hidden=true
        break;
    
      default:
        break;
    }

    this.setState({ data: data });
  }
    
}

export default new OnchangeService()