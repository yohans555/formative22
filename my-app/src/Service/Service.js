import axios from 'axios'

const api_url_get = 'http://localhost:8080/';

class Service {
    
    getData(target){
        const url_target = api_url_get+"get/"+target
        return axios.get(url_target);
    }

    saveData(target,jsondata){
        const url_target = api_url_get+"save/"+target
        console.log('object :>> ', jsondata);
        return  axios.post(url_target,jsondata)
    }


    getJson(state,target){
        let jsondata={};
        switch (target) {
            case "indexmid":
              jsondata = {
                body: state.data.body,
                heading: state.data.heading,
              };
              break;
            case "indexbot":
              jsondata = {
                body: state.data.body,
                subbody: state.data.subbody,
              };
              break;
            case "indextop":
              jsondata = {
                body: state.data.body,
                heading: state.data.heading,
                imageurl: state.data.imageurl,
              };
              break;
            case "aboutleftbot":
              jsondata = {
                body: state.data.body,
                heading: state.data.heading,
              };
              break;
            case "aboutlefttop":
              jsondata = {
                body: state.data.body,
                heading: state.data.heading,
                subbody: state.data.subbody,
              };
              break;
            case "aboutright":
              jsondata = {
                imageurl: state.data.imageurl,
              };
              break;
      
            default:
              jsondata = {};
              break;
          }
          return jsondata;
    }
}

export default new Service()