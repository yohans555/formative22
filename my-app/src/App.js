import { Route, Switch } from 'react-router';
import Header from './Template/Header.js';
import IndexPage from './Main/Index/IndexPage.js';
import FormComponent  from './Main/Form/FormComponent.js';
import Contact from './Main/Contact/Contact.js';

function App() {
  return (
    <>
    <Header />
    <Switch>
      <Route exact path='/' component={IndexPage}/>
      <Route exact path='/form' component={FormComponent}/>
      <Route exact path='/contact' component={Contact}/>
    </Switch>
    </>
  );
}

export default App;
