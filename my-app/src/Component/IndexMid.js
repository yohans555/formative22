import React from 'react';
import Service from '../Service/Service'

class IndexMid extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("indexmid").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render() {
        return (
            <div>
                <div className="centersectiontitle"> 
                <h4 style={{textAlign:'center'}}>What We Do</h4>
                </div>
                <div style={{width:'100%', display:'inline-flex',textAlign: 'left',marginTop:'25px'}}>

                        {
                            this.state.datas.map(
                                data =>
                                <div className="boxcontent" style={{ width:'300px', marginLeft:'30px', marginRight:'10px'}} >
                                    <h3 style={{textAlign:'left'}}>{data.heading}</h3>
                                    <p style={{textAlign:'left'}}>{data.body}</p>
                                    <button style={{background:'#55a2a6', color:'white'}}>Learn More</button>
                                </div>
                                )
                        }
                    
                </div>
            </div>
        )
    }
}

export default IndexMid;