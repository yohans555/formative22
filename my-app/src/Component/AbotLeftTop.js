import React from 'react';
import Service from '../Service/Service'

class AboutLefttop extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("aboutlefttop").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render() {
        return (
            <div>
               
                <div style={{width:'85%',marginLeft:'25px'}}>
                    

                        {
                            this.state.datas.map(
                                data =>
                                <div className="boxcontentabout" style={{marginTop:'25px', marginBottom:'50px'}} >
                                     <div className='sectiontitle'>
                                        <h4 style={{borderLeft: 'solid 6px #55a2a6'}}>{data.heading}</h4>
                                    </div>
                                    <p>{data.body}</p>
                                    <blockquote style={{backgroundColor:'#F5F5F5'}}>
                                       <i>{data.subbody}</i> 
                                    </blockquote>
                                </div>
                                )
                        }
                    
                </div>
            </div>
        )
    }
}

export default AboutLefttop;