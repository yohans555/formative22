import React from 'react';
import Service from '../Service/Service';

class IndexBot extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("indexbot").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render() {
        return (
            <div >
                {
                    this.state.datas.map(
                        data =>
                        <div style={{width: '100%',marginTop:'60px', borderLeft: 'solid 6px #55a2a6', paddingLeft:'16px', color: 'gray', fontFamily: 'serif'}}>
                            <i>"{data.body}"</i><br></br><br></br>
                            <span style={{color:'#55a2a6'}}>- {data.subbody}</span><br></br>
                        </div>
                        )
                }
            </div>
        )
    }
}

export default IndexBot;