import React from 'react';

class SubHeaderIndex extends React.Component {
    render() {
        return (
            <div>
                <div style={{background:'#55a2a6', textAlign: 'center', color:"white", paddingTop:'20px', paddingBottom:'20px'}}>
                    "Vision is the art of seeing what is invisible to others" -Jonathan Swift
                </div>
            </div>
        )
    }
}

export default SubHeaderIndex;