import React from 'react';
import Service from '../Service/Service';

class AboutLeftBot extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("aboutleftbot").then((response)=>{
            this.setState({datas:response.data})
        })
    }


    openData = (param,event)  => {
        let elems = document.getElementsByClassName('para');
        
        for (let index = 0; index < elems.length; index++) {
            const element = elems[index];
            element.style.display='none'
        }

        document.getElementById(param).style.display='block';
                
    }

    render() {
        return (
            <div>
                <div style={{marginRight: '100%', marginLeft:'15px', marginTop:'30px', textAllign: 'left', display: "grid", gridTemplateColumns: "repeat(6, 1fr)", gridGap: 20 }}>
                {
                    this.state.datas.map(
                            data =>
                                <div>
                                    <input id='para' className={data.heading} type='button' style={{marginBottom: '8px', marginTop: '2px', marginRight: '6px', backgroundColor: 'Transparent', border: 'none', color: 'gray'}} value={data.heading} onClick={(e)=>this.openData(data.heading)}></input>
                                </div>
                            )
                }
                </div>

                <div style={{marginLeft:'15px', marginTop:'30px', textAllign: 'left',marginBottom:"20px"}}>
                {
                    this.state.datas.map(
                            data =>
                                    <p id={data.heading} className='para' style={{marginBottom: '2px', marginTop: '2px', display:'none'}}>{data.body}<span id='lol' >a</span></p>
                            )
                }
                </div>

                
           
            </div>
        )
    }
}

export default AboutLeftBot;