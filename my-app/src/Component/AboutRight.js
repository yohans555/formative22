import React from 'react';
import Service from '../Service/Service'

class AboutRight extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("aboutright").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render() {
        return (
            <div>
                <div style={{width:'25%', marginTop:'25px'}}>
                        {
                            this.state.datas.map(
                                data =>
                                <div className="boximage" style={{border:'2px solid #F0F0F0',borderRadius:'8px' ,width:'250px',marginTop:'30px'}} >
                                    <img style={{width:'95%', height:'95%'}} src={data.imageurl} alt={data.imageurl}></img>
                                </div>
                                )
                        }
                    
                </div>
            </div>
        )
    }
}

export default AboutRight;