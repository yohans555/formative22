import React from 'react';
import Service from '../Service/Service';

class IndexTop extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            datas:[]
        }
    }

    componentDidMount(){
        Service.getData("indextop").then((response)=>{
            this.setState({datas:response.data})
        })
    }

    render() {
        return (
            <div>
                <div style={{marginLeft:'50px',marginTop:'30px', textAllign: 'center', display: "grid", gridTemplateColumns: "repeat(4, 1fr)", gridGap: 20 }}>
                {
                    this.state.datas.map(
                            data =>
                                <div style={{textAlign: 'center'}}>
                                    <img src={data.imageurl} style={{width: '50px', height:'50px', marginTop: '8px'}}></img>
                                    <h3 style={{marginBottom: '8px', marginTop: '2px'}}>{data.heading}</h3>
                                    <p style={{marginBottom: '2px', marginTop: '2px'}}>{data.body}</p>
                                </div>
                        
                            )
                }
                </div>
            </div>
        )
    }
}

export default IndexTop;